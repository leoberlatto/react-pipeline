import { render } from '@testing-library/react';
import App from '../App';

describe('App Component Behavior', () => {

    test('Should have Home class in default App rendering (Home as default homepage)', () => {
        const {container} = render(<App/>);
        expect(container.firstChild).toHaveClass('Home');
    });

});