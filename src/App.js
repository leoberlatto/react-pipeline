import './App.css';

import Usuario from './Components/Usuario';
import Home from './Components/Home';
import Login from './Components/Login/Login';
import Sobre from './Components/Sobre';

import {BrowserRouter, Routes, Route} from 'react-router-dom';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/sobre" element={<Sobre/>}/>
                <Route path="/usuario" element={<Usuario/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
