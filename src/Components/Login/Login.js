import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../../Assets/logo.png';
import './Login.css';

function Login() {
    return (
        <div className="App">
            <header className="App-header">
                <div className='App-back'>
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
            ENTRAR
                    </p>
                    <div className='input-email'>
                        <input 
                            type="text"
                            placeholder="Email"
                        />
                    </div>
                    <div className='input-senha'>
                        <input 
                            type="text"
                            placeholder="Senha"
                        />
                    </div>
                    <Link to="/">Esqueci minha senha</Link>
                </div>
        
            </header>
        </div>
    );
}

export default Login;